FROM node:14.15.0-alpine3.10 as builder
WORKDIR /app
COPY package*.json ./
RUN  npm install
COPY . .
RUN npm run build
CMD ["npm", "start"]

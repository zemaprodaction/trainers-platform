import { environment as defaultEnvironment } from './environment.defaults';
export const environment = {
  ...defaultEnvironment,
  production: true,
  api: {
    baseUrl: 'https://onlinegym.space'
  }
};

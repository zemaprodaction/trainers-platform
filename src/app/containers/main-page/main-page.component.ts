import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {Trainer} from "../../share/entities/trainer";
import {User} from "../../share/entities/user";
import {TrainersService} from "../../core/fasades/trainers.service";
import {filter, map, switchMap, tap} from "rxjs/operators";
import {UserService} from "../../core/fasades/user.service";
import {RouterService} from "../../core/fasades/router.service";

@Component({
    selector: 'app-main-page',
    templateUrl: './main-page.component.html',
    styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

    public trainer: Observable<Trainer>;
    public subscriptionUrl: Observable<string>;
    public users: Observable<User[]>;

    constructor(private trainersService: TrainersService,
                private routerService: RouterService,
                private userService: UserService) {
        this.trainer = this.trainersService.getTrainer();
        this.subscriptionUrl = this.trainer.pipe(
            filter(trener => trener.subscribeUrl !== null && trener.subscribeUrl.subscriptionUrl !== null),
            map(trener => trener.subscribeUrl.subscriptionUrl),
            filter(str => str !== null)
        );
        this.users = this.trainer.pipe(
            filter(trainer => trainer !== null && trainer.userList.length !== 0),
            map(tr => tr.userList),
            tap(userList => userService.addUsers(userList)),
            switchMap(_ => userService.getUsers())
        )
    }

    ngOnInit(): void {
        this.trainersService.loadTrainer();
    }

    goToUserPage(user: User) {
        this.routerService.goToPage(['user', user.id])
    }
}

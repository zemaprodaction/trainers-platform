import {environment} from "../../environments/environment";

class RootConstants {
    public static GATEWAY = `${environment.api.baseUrl}`;
}

export class TrainersApiConstants {
    public static ROOT_USER_SERVICE = `${RootConstants.GATEWAY}/api/users`;
    public static GET_SUBSCRIBE_URL = `${TrainersApiConstants.ROOT_USER_SERVICE}/trainer/subscribe_url`;
    public static GET_TRAINER_USERS = `${TrainersApiConstants.ROOT_USER_SERVICE}/trainer/users`;
    public static GET_TRAINER = `${TrainersApiConstants.ROOT_USER_SERVICE}/trainer`;
}

export class StatisticEndpointConstant {
    public static ROOT_STATISTIC_SERVICE = `${RootConstants.GATEWAY}/statistic`;
    public static GET_USER_TRAINING_INFO = `${StatisticEndpointConstant.ROOT_STATISTIC_SERVICE}/user/training-info`;
    public static WORKOUT_PROGRAM_STATISTIC = `${StatisticEndpointConstant.ROOT_STATISTIC_SERVICE}/workoutProgram`;
    public static ROOT_WORKOUT_PROGRAM_SERVICE = `${StatisticEndpointConstant.ROOT_STATISTIC_SERVICE}/user_workout_instance`;
}
export class WorkoutProgramApiConstants {
    public static ROOT_WORKOUT_PROGRAM_SERVICE = `${RootConstants.GATEWAY}/workout-program`;
    public static WORKOUT_PROGRAM = `${WorkoutProgramApiConstants.ROOT_WORKOUT_PROGRAM_SERVICE}/workoutProgram`;
}


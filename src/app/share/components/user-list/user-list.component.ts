import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../../entities/user";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  @Input()
  userList: User[] = []

  @Output()
  userClick: EventEmitter<User> = new EventEmitter<User>()

  constructor() { }

  ngOnInit(): void {
  }

  chooseUser(user: User){
    this.userClick.emit(user)
  }

}

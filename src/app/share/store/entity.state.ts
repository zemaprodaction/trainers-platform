import {createSelector, StateContext} from '@ngxs/store';
import {tap} from 'rxjs/operators';
import {CrudOperations} from "../../core/service-api/crud-operations";
import {RouterStateModel} from "../../router-state.model";
import {RouterState} from "@ngxs/router-plugin";


export const arrayToObject = (entities: any) => entities.reduce((obj: any, entity: any) => ({...obj, [entity.id]: entity}), {});
export const arrayToObjectWithCustomId = (entities: any, idName: string) => entities.reduce((obj: any, entity: any) =>
    ({...obj, [entity[idName]]: entity}), {});

export const defaultsEntityState = {
    ids: [],
    entities: {},
    routerSelectedParams: '',
    loaded: false,
    loading: false,
    idParam: 'id'
};


export interface EntityStateModel<T> {
    ids: string[];
    entities: {
        [id: string]: T;
    };
    routerSelectedParams: string;
    loaded: boolean;
    loading: boolean;
    idParam: string;
}

interface Entity {
    [idParamName: string]: string
}

export class EntityState<T> {

    constructor(private crudOperations: CrudOperations<T, string>) {
    }

    static getEntitiesAsArray<R>() {
        return createSelector([this], (state: EntityStateModel<R>) =>
            Object.keys(state.entities).map(key => state.entities[key]));
    }

    static getEntities<R>() {
        return createSelector([this], (state: EntityStateModel<R>) => state.entities);
    }

    static getLoading<R>() {
        return createSelector([this], (state: EntityStateModel<R>) => state.loading);
    }

    static getSelectedEntity<R>() {
        return createSelector([this, RouterState.state],
            (state: EntityStateModel<R>, routerState: RouterStateModel) => state.entities &&
                state.entities[routerState.params[state.routerSelectedParams]]
        );
    }

    static getEntityById<R>(entityId: string) {
        return createSelector([this],
            (state: EntityStateModel<R>) => state.entities &&
                state.entities[entityId]);
    }

    static getEntityByListId<R>(entityIdList: string[]) {
        return createSelector([this],
            (state: EntityStateModel<R>) => {
                if (state.entities) {
                    return entityIdList.map(id => state.entities[id]);
                } else {
                    return []
                }
            });
    }

    public addOneWithId(ctx: StateContext<EntityStateModel<T>>, entity: T, id: string) {
        const state = ctx.getState();
        const newIds = [...state.ids, id];
        const newEntities = {...state.entities, [id]: entity};
        ctx.patchState({
            ids: newIds,
            entities: newEntities
        });
    }

    public addOne(ctx: StateContext<EntityStateModel<T>>, entity: T) {
        const state = ctx.getState();
        // @ts-ignore
        const id: string = entity[state.idParam];
        const newIds = [...state.ids, id];
        // @ts-ignore
        const newEntities = {...state.entities, [entity[state.idParam]]: entity};
        ctx.patchState({
            ids: newIds,
            entities: newEntities
        });
    }

    public addAll<T extends Entity>(ctx: StateContext<EntityStateModel<T>>, entityList: T[]) {
        // @ts-ignore
        ctx.patchState({
            entities: arrayToObject(entityList),
            ids: entityList.map<string>(entity => entity[ctx.getState().idParam])
        });
    }

    public addAllWithCustomId<T extends Entity>(ctx: StateContext<EntityStateModel<T>>, entityList: T[], idParamName: string) {
        // @ts-ignore
        ctx.patchState({
            entities: arrayToObjectWithCustomId(entityList, idParamName),
            ids: entityList.map<string>(entity => {
                return entity[idParamName]
            })
        });
    }

    public loadById(ctx: StateContext<EntityStateModel<T>>, id: string) {
        if (!ctx.getState().entities[id] && !ctx.getState().loading) {
            this.startLoading(ctx);
            return this.crudOperations.findOne(id).pipe(
                tap((entity) => {
                    const state = ctx.getState();
                    const idParamName = ctx.getState().idParam
                    const newState = {
                        ...state,
                        entities: {...state.entities, [(entity as any)[idParamName]]: entity},
                        ids: [...state.ids, (entity as any)[idParamName]],
                        loading: false
                    };
                    ctx.setState(newState);
                })
            );
        } else {
            return null
        }
    }

    public loadByIdList(ctx: StateContext<EntityStateModel<T>>, idList: string[]) {
        idList = Array.from(new Set(idList));
        return this.crudOperations.findByIdList(idList).pipe(
            tap(entityList => {
                const state = ctx.getState();
                const newState = this.getNewStateWithNewEntityList(state, entityList);
                ctx.setState(newState);
            })
        );
    }

    public load(ctx: StateContext<EntityStateModel<T>>) {
        if (!ctx.getState().loaded && !ctx.getState().loading) {
            this.startLoading(ctx);
            return this.crudOperations.findAll().pipe(
                tap(entityList => {
                    const state = ctx.getState();
                    const newState = this.getNewStateWithNewEntityList(state, entityList);
                    ctx.setState(newState);
                    ctx.patchState({
                        loaded: true,
                        loading: false
                    });
                })
            );
        } else {
            return null
        }
    }

    public delete(ctx: StateContext<EntityStateModel<T>>, exerciseId: string) {
        const state = ctx.getState();
        return this.crudOperations.delete(exerciseId).pipe(
            tap(entityId => {
                const newEntities = {};
                delete state.entities[entityId];
                const newState = {
                    ...state,
                    ids: state.ids.filter(id => id !== entityId),
                    entities: newEntities,
                    loading: false,
                    loaded: true
                };

                ctx.setState(
                    {...newState}
                );
            })
        );
    }

    private startLoading(ctx: StateContext<EntityStateModel<T>>) {
        ctx.patchState({
            loading: true
        });
    }

    private getNewStateWithNewEntityList(state: EntityStateModel<T>, entityList: any[]) {
        const loadEntity = arrayToObject(entityList);
        return {
            ...state,
            entities: {...state.entities, ...loadEntity},
            ids: entityList.map<string>(entity => entity.id)
        };
    }
}

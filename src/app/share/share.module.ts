import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CopyButtonComponent} from './components/copy-button/copy-button.component';
import {ClipboardModule} from "@angular/cdk/clipboard";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {UserListComponent} from './components/user-list/user-list.component';
import {MatListModule} from "@angular/material/list";
import {FlexModule} from '@angular/flex-layout';
import {BackgroundCoverDirective} from "./directive/background-cover.directive";
import * as fromPipes from './pipes';
import {MatCardModule} from "@angular/material/card";
import {UserStateModule} from "../shared-states/user-state/user-state.module";


@NgModule({
    declarations: [
        CopyButtonComponent,
        UserListComponent,
        BackgroundCoverDirective,
        ...fromPipes.pipes
    ],
    imports: [
        CommonModule,
        ClipboardModule,
        FormsModule,
        MatIconModule,
        MatButtonModule,
        MatListModule,
        FlexModule,
        MatCardModule,
        UserStateModule,
        ReactiveFormsModule
    ],
    exports: [
        CopyButtonComponent,
        UserListComponent,
        BackgroundCoverDirective,
        ...fromPipes.pipes,
        CommonModule,
        ClipboardModule,
        FormsModule,
        MatIconModule,
        MatButtonModule,
        MatListModule,
        FlexModule,
        MatCardModule,
    ],
})
export class ShareModule { }

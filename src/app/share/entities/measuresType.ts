export interface MeasuresType {
  id: string;
  name: string;
  measurePrefix: string;
  measurePrefixLocalized: string;
}

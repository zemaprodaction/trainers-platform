import {UserWorkoutProgram} from "./user-workout-program";

export interface UserTrainingInfo {
    userId: String
    userWorkoutProgram: UserWorkoutProgram
}

import {WorkoutInstancePhaseEntity} from "./workout-instance-phase-entity";

export interface UserWorkoutInstanceEntity {
    id: string;
    trainingWorkoutId?: string;
    userId?: string;
    listOfWorkoutPhases?: WorkoutInstancePhaseEntity[];
    status?: "NOT_STARTED" | "IN_PROGRESS" | "FINISHED";
    name?: string;
    percentComplete: number;
    workoutDifficalt: number
}

import {User} from "./user";
import {TrainerSubscription} from "./trainer-subscription";

export interface Trainer {
    id: string,
    userList: User[],
    subscribeUrl: TrainerSubscription
}

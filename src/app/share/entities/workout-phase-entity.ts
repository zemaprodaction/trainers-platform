import {Exercise} from "./exercise";

export interface WorkoutPhaseEntity {

  listOfExercises: string[];
  name: string;

}

export interface WorkoutPhase extends WorkoutPhaseEntity {
  listOfExercisesFetched: Exercise[];
}

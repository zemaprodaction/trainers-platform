import {WorkoutInstancePhaseEntity} from "./workout-instance-phase-entity";

export interface UserWorkoutInstance {
    id: string;
    workoutProgramId: string;
    userId: string;
    listOfWorkoutPhases: WorkoutInstancePhaseEntity[]
}

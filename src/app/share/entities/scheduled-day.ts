import {UserWorkoutInstanceEntity} from './user-workout-instance-entity';

export interface ScheduledDay {
  id: string,
  userWorkoutInstanceList: UserWorkoutInstanceEntity[];
  date: string
}

export interface ImageBundle {
  mobileImageLink: string;
  webImageLink: string;
}

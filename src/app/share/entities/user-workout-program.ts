import {WorkoutProgramScheduler} from "./workout-program-scheduler";

export interface UserWorkoutProgram {
    id: string,
    trainingWorkoutProgramId: string,
    workoutProgramScheduler: WorkoutProgramScheduler,
    userId: string,
    status: 'NOT_STARTED' | 'IN_PROGRESS' | 'FINISHED'
}

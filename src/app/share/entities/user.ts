import {UserTrainingInfo} from "./user-training-info";

export interface User {
    id: string,
    preferredName: string,
    userTrainingInfo: UserTrainingInfo
}

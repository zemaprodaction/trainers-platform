import {ImageBundle} from "./image-bundle";

export interface WorkoutCommon {
  name?: string;
  description?: string;
  imageBundle?: ImageBundle;
}

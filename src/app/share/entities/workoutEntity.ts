import {WorkoutCommon} from "./workout-common";
import {WorkoutProgramCharacteristic} from "./workout-programs";
import {WorkoutPhase, WorkoutPhaseEntity} from "./workout-phase-entity";


export interface WorkoutEntity {
  id: string;
  common?: WorkoutCommon;
  listOfPhases: WorkoutPhaseEntity[];
  duration?: number;
  characteristic?: WorkoutProgramCharacteristic;
}

export interface Workout {
  id: string;
  common?: WorkoutCommon;
  characteristic?: WorkoutProgramCharacteristic;
  listOfPhases: WorkoutPhase[];
  duration?: number;
}







export interface CharacteristicProperty {
  name?: string;
  value: string | string[];
  nativeValue: string | string[];
}

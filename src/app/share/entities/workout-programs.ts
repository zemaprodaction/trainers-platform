import {CharacteristicProperty} from "./characteristic-property";
import {WorkoutCommon} from "./workout-common";
import {Workout} from "./workoutEntity";
import {UserWorkoutInstanceEntity} from "./user-workout-instance-entity";

export enum WorkoutProgramCharacteristicConstants {
  workoutType = 'workoutType',
  workoutDifficulty = 'workoutDifficulty',
  equipmentNecessity = 'equipmentNecessity',
  workoutPlaces = 'workoutPlaces',
}

export interface WorkoutProgramCharacteristic {
  workoutType: CharacteristicProperty;
  workoutDifficulty: CharacteristicProperty;
  equipmentNecessity: CharacteristicProperty;
  workoutPlaces: CharacteristicProperty;
}

export interface WorkoutProgramEntity {
  id: string;
  common?: WorkoutCommon;
  listOfWeeks?: WorkoutWeekEntity[];
  workoutsOnProgram?: string[];
  amountOfDays?: number;
  amountWorkoutInWeek?: Range;
  characteristic?: WorkoutProgramCharacteristic;
}

export interface WorkoutProgram {
  id: string;
  common?: WorkoutCommon;
  listOfWeeks?: WorkoutWeekEntity[];
  workoutsOnProgram?: Workout[];
  amountOfDays?: number;
  amountWorkoutInWeek?: Range;
  characteristic?: WorkoutProgramCharacteristic;
}

export interface WorkoutWeekEntity {
  listOfDays: WorkoutDayEntity[];
  weekNumber?: number;
  isComplete?: string;
}

export interface WorkoutDayEntity {
  listOfScheduledWorkouts?: UserWorkoutInstanceEntity[];
  dayIndex?: number;
  type?: string;
  unFinishedWorkouts?: number;
  numberOfDay?: number;
}






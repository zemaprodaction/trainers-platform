import {ScheduledDay} from "./scheduled-day";

export interface WorkoutProgramScheduler {
    id: String,
    listOfDays: ScheduledDay[];
}


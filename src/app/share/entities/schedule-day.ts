import {UserWorkoutInstanceEntity} from "./user-workout-instance-entity";

export interface ScheduleDay {
    scheduledWorkoutList: UserWorkoutInstanceEntity[];
    date: string
}

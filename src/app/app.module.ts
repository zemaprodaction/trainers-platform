import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {CoreModule} from "./core/core.module";
import {CommonModule} from "@angular/common";
import {MatToolbarModule} from "@angular/material/toolbar";
import {NgxsModule} from "@ngxs/store";
import {AppState} from "./core/store/states/app.state";
import {environment} from "../environments/environment";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {ClipboardModule} from "@angular/cdk/clipboard";
import {ShareModule} from "./share/share.module";
import {routing} from "./app.routing";
import {NgxsReduxDevtoolsPluginModule} from "@ngxs/devtools-plugin";
import {NgxsRouterPluginModule, RouterStateSerializer} from "@ngxs/router-plugin";
import {MainPageComponent} from './containers/main-page/main-page.component';
import {CustomRouterStateSerializer} from "./router-state.serializer";

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent
  ],
  imports: [
    CoreModule,
    BrowserModule,
    CommonModule,
    MatToolbarModule,
    routing,
    NgxsModule.forRoot([AppState], {
      developmentMode: !environment.production,
      selectorOptions: {
        suppressErrors: false,
        injectContainerState: false
      }
    }),
    NgxsRouterPluginModule.forRoot(),
    NgxsReduxDevtoolsPluginModule.forRoot({
      name: 'NGXS store',
      disabled: environment.production
    }),
    HttpClientModule,
    FormsModule,
    ClipboardModule,
    ShareModule,
  ],
  providers: [
    {provide: RouterStateSerializer, useClass: CustomRouterStateSerializer},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxsModule} from "@ngxs/store";
import {UserStates} from "./state";


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        NgxsModule.forFeature(UserStates)
    ]
})
export class UserStateModule {
}

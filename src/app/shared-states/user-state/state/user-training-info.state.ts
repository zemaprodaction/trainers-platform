import {Action, createSelector, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {UserStatisticApiService} from "../../../core/service-api/user-statistic-api.service";
import {tap} from "rxjs/operators";
import {UserTrainingInfo} from "../../../share/entities/user-training-info";
import {UserTrainingInfoActions} from "../actions/user-training-info.actions";

export interface UserTrainingInfoStateModel {
    entities: {
        [userId: string]: UserTrainingInfo;
    },
    ids: string[]
}

@State<UserTrainingInfoStateModel>({
    name: 'userTrainingInfo',
    defaults: {
        entities: {},
        ids: []
    }
})
@Injectable()
export class UserTrainingInfoState {

    constructor(private userStatisticService: UserStatisticApiService) {
    }

    static getUsersTraingInfo(userId: string) {
        return createSelector([UserTrainingInfoState],
            (state: UserTrainingInfoStateModel) => state.entities &&
                state.entities[userId]);
    }

    @Action(UserTrainingInfoActions.LoadUserTrainingInfo)
    public loadUserTrainingInfo(ctx: StateContext<UserTrainingInfoStateModel>,
                                {userId}: UserTrainingInfoActions.LoadUserTrainingInfo) {
        return this.userStatisticService.getUserInfo(userId).pipe(
            tap(userTrainingInfo => {
                ctx.patchState({
                    entities: {...ctx.getState().entities, [userId]: userTrainingInfo},
                    ids: [...ctx.getState().ids, userId]
                })
            })
        )
    }
}

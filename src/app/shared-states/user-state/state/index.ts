import {UserState} from "./user.state";
import {UserTrainingInfoState} from "./user-training-info.state";

export const UserStates = [UserState, UserTrainingInfoState]

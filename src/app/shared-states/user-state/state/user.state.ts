// eslint-disable-next-line @typescript-eslint/no-empty-interface

import {arrayToObject, defaultsEntityState, EntityStateModel} from "../../../share/store/entity.state";
import {User} from "../../../share/entities/user";
import {Action, Selector, State, StateContext} from "@ngxs/store";
import {Injectable} from "@angular/core";
import {UserActions} from "../actions/user.actions";
import {UserTrainingInfoState} from "./user-training-info.state";

export interface UserStateModel extends EntityStateModel<User> {
}

@State<UserStateModel>({
    name: 'user',
    defaults: {
        ...defaultsEntityState,
        routerSelectedParams: 'userId'
    },
    children: [UserTrainingInfoState]
})
@Injectable()
export class UserState {

    @Selector([UserState])
    public static getUsers(state: UserStateModel) {
        return state.ids.map(id => state.entities[id]);
    }

    @Action(UserActions.AddUsers)
    public addWorkoutProgramById(ctx: StateContext<UserStateModel>,
                                 {userList}: UserActions.AddUsers) {
        ctx.patchState({
            entities: arrayToObject(userList),
            ids: userList.map(u => u.id)
        })
    }
}

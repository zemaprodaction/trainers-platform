export namespace CurrentWorkoutProgramStatisticActions {

    export class LoadCurrentWorkoutProgramStatistic {

        public static readonly type = '[Current Workout Program Statistic Actions] Load';

        constructor(public userId: string, public workoutProgramId: string) {
        }
    }
}









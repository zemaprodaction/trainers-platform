import {Action, createSelector, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {CurrentWorkoutProgramStatistic} from "../../../share/entities/current-workout-program-statistic";
import {CurrentWorkoutProgramStatisticActions} from "../actions/current-workout-program-statistic";
import {UserStatisticApiService} from "../../../core/service-api/user-statistic-api.service";
import {tap} from "rxjs/operators";

export interface CurrentWorkoutProgramStatisticStateModel {
    entities: {
        [userId: string]: CurrentWorkoutProgramStatistic
    },
    ids: string[]
}

@State<CurrentWorkoutProgramStatisticStateModel>({
    name: 'currentWorkoutProgramStatistic',
    defaults: {
        entities: {},
        ids: []
    }
})
@Injectable()
export class CurrentWorkoutProgramStatisticState {

    constructor(private serviceApi: UserStatisticApiService) {
    }

    static selectCurrentWorkoutProgramStatisticByUserId(userId: string) {
        return createSelector([CurrentWorkoutProgramStatisticState],
            (state: CurrentWorkoutProgramStatisticStateModel) => state.entities[userId]);
    }

    @Action(CurrentWorkoutProgramStatisticActions.LoadCurrentWorkoutProgramStatistic)
    public loadExercise(ctx: StateContext<CurrentWorkoutProgramStatisticStateModel>,
                        {userId, workoutProgramId}: CurrentWorkoutProgramStatisticActions.LoadCurrentWorkoutProgramStatistic) {
        return this.serviceApi.getUserCurrentWorkoutProgramStatistic(userId, workoutProgramId).pipe(
            tap(stat => {
                const state = ctx.getState()
                ctx.patchState({
                    entities: {...state.entities, [userId]: stat},
                    ids: [...state.ids, userId]
                })
            })
        )
    }

}

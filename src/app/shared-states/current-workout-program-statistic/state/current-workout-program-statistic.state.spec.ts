import {NgxsModule, Store} from '@ngxs/store';
import {fakeAsync, TestBed, tick, waitForAsync} from '@angular/core/testing';
import {of} from 'rxjs';
import {HttpClientModule} from '@angular/common/http';
import {CurrentWorkoutProgramStatisticState} from "./current-workout-program-statistic.state";
import {CurrentWorkoutProgramStatisticActions} from "../actions/current-workout-program-statistic";
import {UserStatisticApiService} from "../../../core/service-api/user-statistic-api.service";
import {CurrentWorkoutProgramStatistic, UserWorkoutProgramStatistic} from "../../../share/entities/current-workout-program-statistic";
import SpyObj = jasmine.SpyObj;
import createSpyObj = jasmine.createSpyObj;

describe('Current Workout Program Statistic State', () => {
    let store: Store;
    let userStatisticApiService: SpyObj<UserStatisticApiService>;
    const userId = "testUserId";
    const workoutProgramId = "testWpId";
    const userWorkoutProgramStatistic: UserWorkoutProgramStatistic = {
        workoutProgramId: workoutProgramId,
        finishedWorkoutInstance: [],
        userId: userId
    }
    const currentWorkoutProgramStatisticTestData: CurrentWorkoutProgramStatistic = {
        averagePercentComplete: 80,
        percentDone: 50,
        workoutProgramStatistic: userWorkoutProgramStatistic
    }
    beforeEach(waitForAsync(() => {
        userStatisticApiService = createSpyObj('UserStatisticApiService', ['getUserCurrentWorkoutProgramStatistic'])
        userStatisticApiService.getUserCurrentWorkoutProgramStatistic.and.returnValue(of(currentWorkoutProgramStatisticTestData))
        TestBed.configureTestingModule({
            imports: [NgxsModule.forRoot([CurrentWorkoutProgramStatisticState],
                {
                    selectorOptions: {
                        suppressErrors: false,
                        injectContainerState: false
                    }
                }),
                HttpClientModule],
            providers: [{provide: UserStatisticApiService, useValue: userStatisticApiService}]
        }).compileComponents();
        store = TestBed.inject(Store);
    }));

    describe('when dispatch Load Current Workout Program Statistic', () => {
        it('should load data', fakeAsync(() => {
            store.dispatch(new CurrentWorkoutProgramStatisticActions.LoadCurrentWorkoutProgramStatistic(userId, workoutProgramId));
            tick();
            const currentWorkoutProgramStatistic = store.selectSnapshot(CurrentWorkoutProgramStatisticState
                .selectCurrentWorkoutProgramStatisticByUserId(userId))
            expect(currentWorkoutProgramStatistic).toEqual(currentWorkoutProgramStatisticTestData);
        }));
    });

});

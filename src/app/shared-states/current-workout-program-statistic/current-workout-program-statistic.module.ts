import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxsModule} from "@ngxs/store";
import {CurrentWorkoutProgramStatisticState} from "./state/current-workout-program-statistic.state";


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgxsModule.forFeature([CurrentWorkoutProgramStatisticState])
  ]
})
export class CurrentWorkoutProgramStatisticModule { }

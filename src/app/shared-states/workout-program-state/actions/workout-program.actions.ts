
export namespace WorkoutProgramActions {

  export class LoadWorkoutProgram {

    public static readonly type = '[Workout Program] Load';

    constructor(public workoutProgramId: string) {
    }
  }
}









// eslint-disable-next-line @typescript-eslint/no-empty-interface
import {WorkoutProgramEntity} from "../../../share/entities/workout-programs";
import {defaultsEntityState, EntityState, EntityStateModel} from "../../../share/store/entity.state";
import {Action, Selector, State, StateContext} from "@ngxs/store";
import {Injectable} from "@angular/core";
import {WorkoutProgramServiceApi} from "../../../core/service-api/workout-program-service-api.service";
import {WorkoutProgramActions} from "../actions/workout-program.actions";

export interface WorkoutProgramStateModel extends EntityStateModel<WorkoutProgramEntity> {
}

@State<WorkoutProgramStateModel>({
    name: 'workoutProgram',
    defaults: {
        ...defaultsEntityState,
        routerSelectedParams: 'workoutProgramId'
    },
})
@Injectable()
export class WorkoutProgramState extends EntityState<WorkoutProgramEntity> {

    constructor(private workoutProgramService: WorkoutProgramServiceApi) {
        super(workoutProgramService);
    }


    @Selector([WorkoutProgramState.getSelectedEntity()])
    public static getSelectedWorkoutPrograms(workoutProgram: WorkoutProgramEntity) {
        return workoutProgram;
    }


    @Action(WorkoutProgramActions.LoadWorkoutProgram)
    public addWorkoutProgramById(ctx: StateContext<EntityStateModel<WorkoutProgramEntity>>,
                                 {workoutProgramId}: WorkoutProgramActions.LoadWorkoutProgram) {
        return this.loadById(ctx, workoutProgramId);
    }


}

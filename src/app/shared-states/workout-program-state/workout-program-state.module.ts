import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgxsModule} from "@ngxs/store";
import {WorkoutProgramState} from "./state/workout-program.state";


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        NgxsModule.forFeature([WorkoutProgramState])
    ]
})
export class WorkoutProgramStateModule {
}

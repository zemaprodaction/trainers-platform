import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserSchedulerComponent} from './containers';
import {routing} from "./user-schedule-router";
import {ShareModule} from '../share/share.module';
import {WorkoutProgramPresenterComponent} from './component/workout-program-presenter/workout-program-presenter.component';
import {WorkoutProgramStateModule} from "../shared-states/workout-program-state/workout-program-state.module";
import {NgxsModule} from "@ngxs/store";
import {UserStates} from "../shared-states/user-state/state";
import {CurrentWorkoutProgramStatisticModule} from "../shared-states/current-workout-program-statistic/current-workout-program-statistic.module";
import { WorkoutProgramSchedulerComponent } from './component/workout-program-scheduler/workout-program-scheduler.component';


@NgModule({
    declarations: [
        UserSchedulerComponent,
        WorkoutProgramPresenterComponent,
        WorkoutProgramSchedulerComponent,
    ],
    imports: [
        CommonModule,
        ShareModule,
        routing,
        WorkoutProgramStateModule,
        CurrentWorkoutProgramStatisticModule,
        NgxsModule.forFeature(UserStates)
    ]
})
export class UserScheduleModule {
}

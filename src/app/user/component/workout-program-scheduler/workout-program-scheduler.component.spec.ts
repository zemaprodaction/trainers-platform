import {ComponentFixture, TestBed} from '@angular/core/testing';

import {WorkoutProgramSchedulerComponent} from './workout-program-scheduler.component';
import {By} from "@angular/platform-browser";
import {WorkoutProgramScheduler} from "../../../share/entities/workout-program-scheduler";
import {MatListModule} from "@angular/material/list";

describe('WorkoutProgramSchedulerComponent', () => {
    let component: WorkoutProgramSchedulerComponent;
    let fixture: ComponentFixture<WorkoutProgramSchedulerComponent>;
    const scheduler: WorkoutProgramScheduler = {
        calendar: {
            '10': {scheduledWorkoutList: [], date: "10"},
            '20': {scheduledWorkoutList: [], date: "10"},
            '30': {scheduledWorkoutList: [], date: "10"},
        }
    }
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [WorkoutProgramSchedulerComponent],
            imports: [MatListModule]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(WorkoutProgramSchedulerComponent);
        component = fixture.componentInstance;
        component.scheduler = scheduler

    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should contain mat-nav-list', () => {
        fixture.detectChanges();
        const matNavList = fixture.nativeElement.querySelector('mat-nav-list')
        expect(matNavList).toBeTruthy();
    });

    describe('when scheduler has 3 days', () => {
        it('should contains 3 mat-list-item', () => {
            fixture.detectChanges()
            const matListItems = fixture.debugElement.queryAll(By.css('mat-list-item'))
            expect(matListItems.length).toEqual(3)
        })
    })
});

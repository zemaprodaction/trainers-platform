import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {WorkoutProgramScheduler} from "../../../share/entities/workout-program-scheduler";
import {UserWorkoutInstanceEntity} from "../../../share/entities/user-workout-instance-entity";

@Component({
    selector: 'app-workout-program-scheduler',
    templateUrl: './workout-program-scheduler.component.html',
    styleUrls: ['./workout-program-scheduler.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WorkoutProgramSchedulerComponent implements OnInit {

    @Input()
    scheduler!: WorkoutProgramScheduler
    @Output()
    chooseWorkoutInstance: EventEmitter<UserWorkoutInstanceEntity> = new EventEmitter<UserWorkoutInstanceEntity>()

    constructor() {
    }

    ngOnInit(): void {
    }

    editWorkoutInstance(workoutInstacne: UserWorkoutInstanceEntity) {
        this.chooseWorkoutInstance.emit(workoutInstacne)
    }

    getDifficalty(workoutDifficalt: number) {
        switch (workoutDifficalt) {
            case 1:
                return "Очень легко";
            case 2:
                return "Легко";
            case 3:
                return "Нормально";
            case 4:
                return "Сложно";
            case 5:
                return "Очень сложно";
            default:
                return ""
        }
    }
}

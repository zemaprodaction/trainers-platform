import {Component, Input, OnInit} from '@angular/core';
import {WorkoutProgramEntity} from "../../../share/entities/workout-programs";

@Component({
  selector: 'app-workout-program-presenter',
  templateUrl: './workout-program-presenter.component.html',
  styleUrls: ['./workout-program-presenter.component.css']
})
export class WorkoutProgramPresenterComponent implements OnInit {

  @Input()
  workoutProgram?: WorkoutProgramEntity;
  @Input()
  workoutProgramPercentDone: number = 0;

  constructor() {
  }

  ngOnInit(): void {
  }

}

import {RouterModule, Routes} from '@angular/router';
import * as fromContainers from './containers';
import {ModuleWithProviders} from '@angular/core';
import {userScheduleRoutesNames} from "./user-schedule-routes.names";

const TRAINING_ROUTES: Routes = [
  {
    path: `${userScheduleRoutesNames.USER_ID}`,
    component: fromContainers.UserSchedulerComponent,
  }
];


export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(TRAINING_ROUTES);

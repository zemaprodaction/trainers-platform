import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {UserTrainingInfo} from "../../../share/entities/user-training-info";
import {WorkoutProgramService} from "../../../core/fasades/workout-program.service";
import {WorkoutProgramStatisticService} from "../../../core/fasades/workout-program-statistic.service";
import {UserStatisticService} from "../../../core/fasades/user-statistic.service";
import {filter, switchMap, tap} from "rxjs/operators";
import {WorkoutProgramEntity} from "../../../share/entities/workout-programs";
import {UserWorkoutInstanceEntity} from "../../../share/entities/user-workout-instance-entity";
import {RouterService} from "../../../core/fasades/router.service";

@Component({
    selector: 'app-user-scheduler',
    templateUrl: './user-scheduler.component.html',
    styleUrls: ['./user-scheduler.component.css']
})
export class UserSchedulerComponent implements OnInit {

    userTrainingInfo: Observable<UserTrainingInfo>
    workoutProgram$: Observable<WorkoutProgramEntity>
    // currentWorkoutProgramStatistic: Observable<CurrentWorkoutProgramStatistic>

    constructor(private userStatisticService: UserStatisticService,
                private workoutProgramService: WorkoutProgramService,
                private workoutProgramStatisticService: WorkoutProgramStatisticService,
                private routerService: RouterService,
                private route: ActivatedRoute) {
        const userId = this.route.snapshot.params["userId"]
        userStatisticService.loadTrainingInfo(userId)
        this.userTrainingInfo = userStatisticService.getUserTrainingInfo(userId)
        // this.currentWorkoutProgramStatistic = this.userTrainingInfo.pipe(
        //     filter(info => info !== null && info !== undefined && info.userWorkoutProgram !== null),
        //     tap(info => userStatisticService.loadUserWorkoutProgramCurrentStatistic(userId, info.userWorkoutProgram.trainingWorkoutProgramId)),
        //     switchMap(info => userStatisticService.getUserCurrentWorkoutProgramStatistic(userId))
        // )
        this.workoutProgram$ = this.userTrainingInfo.pipe(
            filter(u => u !== null && u !== undefined),
            tap(u => this.workoutProgramService.loadWorkoutProgram(u.userWorkoutProgram.trainingWorkoutProgramId)),
            switchMap(u => this.workoutProgramService.getWorkoutProgram(u.userWorkoutProgram.trainingWorkoutProgramId))
        )
    }

    ngOnInit(): void {

    }

    goToEditWorkoutInstance(workoutInstanceEntity: UserWorkoutInstanceEntity) {
        this.routerService.goToPage(['edit', workoutInstanceEntity.id])
    }
}

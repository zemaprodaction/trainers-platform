import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {appRoutesNames} from './routes.names';
import {MainPageComponent} from "./containers/main-page/main-page.component";

export const APP_ROUTES: Routes = [
    {
        path: `${appRoutesNames.USER}`,
        loadChildren: () => import('./user/user-schedule.module').then(m => m.UserScheduleModule),
    },
    {
        path: `${appRoutesNames.WORKOUT_INSTANCE_EDITOR}`,
        loadChildren: () => import('./workout-set-editor/workout-set-editor.module').then(m => m.WorkoutSetEditorModule),
    },
    {
        path: '',
        component: MainPageComponent
    },
    {path: '**', pathMatch: 'full', redirectTo: `/`},
];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forRoot(
    APP_ROUTES,
);

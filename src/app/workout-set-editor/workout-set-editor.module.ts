import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {routing} from "./workout-set-editor.routing";
import { WorkoutSetEditorComponent } from './container/workout-set-editor/workout-set-editor.component';
import { WorkoutPhaseListComponent } from './component/workout-phase-list/workout-phase-list.component';
import {ShareModule} from "../share/share.module";
import {MatInputModule} from '@angular/material/input';
import { UserWorkoutInstanceComponent } from './component/user-workout-instance/user-workout-instance.component'
import {ReactiveFormsModule} from "@angular/forms";
import { WorkoutSetListComponent } from './component/workout-set/workout-set-list/workout-set-list.component';
import { MeasureListComponent } from './component/measure-list/measure-list.component';

@NgModule({
    declarations: [
    WorkoutSetEditorComponent,
    WorkoutPhaseListComponent,
    UserWorkoutInstanceComponent,
    WorkoutSetListComponent,
    MeasureListComponent
  ],
    imports: [
        CommonModule,
        ShareModule,
        routing,
        MatInputModule,
        ShareModule,
        ReactiveFormsModule
    ]
})
export class WorkoutSetEditorModule {
}

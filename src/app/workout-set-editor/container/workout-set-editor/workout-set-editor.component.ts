import {Component, OnInit} from '@angular/core';
import {UserWorkoutInstanceService} from "../../../core/fasades/user-workout-instance.service";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {UserWorkoutInstance} from "../../../share/entities/user-workout-instance";
import {filter, take} from "rxjs/operators";

@Component({
    selector: 'app-workout-set-editor',
    templateUrl: './workout-set-editor.component.html',
    styleUrls: ['./workout-set-editor.component.css']
})
export class WorkoutSetEditorComponent implements OnInit {

    userWorkoutInstance$!: Observable<UserWorkoutInstance>

    constructor(private userWorkoutInstanceService: UserWorkoutInstanceService, private route: ActivatedRoute) {
        const userWorkoutInstanceId = this.route.snapshot.params['workoutInstanceId']
        this.userWorkoutInstance$ = this.userWorkoutInstanceService.getUserWorkoutInstance(userWorkoutInstanceId).pipe(
            filter(inst => inst !== null)
        )
    }

    ngOnInit(): void {

    }

    updateUserWorkoutInstance(userWorkoutInstance: UserWorkoutInstance) {
        this.userWorkoutInstanceService.updateUserWorkoutInstance(userWorkoutInstance).pipe(
            take(1)
        ).subscribe()
    }
}

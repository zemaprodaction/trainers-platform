import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkoutSetEditorComponent } from './workout-set-editor.component';

describe('WorkoutSetEditorComponent', () => {
  let component: WorkoutSetEditorComponent;
  let fixture: ComponentFixture<WorkoutSetEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkoutSetEditorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkoutSetEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

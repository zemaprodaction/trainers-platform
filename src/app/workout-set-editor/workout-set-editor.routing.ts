import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {WorkoutSetEditorComponent} from "./container/workout-set-editor/workout-set-editor.component";

export const APP_ROUTES: Routes = [
    {
        path: ':workoutInstanceId',
        component: WorkoutSetEditorComponent
    },
];

export const routing: ModuleWithProviders<RouterModule> = RouterModule.forChild(
    APP_ROUTES,
);

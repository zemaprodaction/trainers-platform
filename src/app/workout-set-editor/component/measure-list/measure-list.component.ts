import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {Measure} from "../../../share/entities/measureEntity";

@Component({
    selector: 'app-measure-list',
    templateUrl: './measure-list.component.html',
    styleUrls: ['./measure-list.component.css']
})
export class MeasureListComponent implements OnInit {

    @Input()
    parentGroup!: FormGroup

    @Input()
    measureList!: Measure[]

    measureFormArray: FormArray

    constructor(private fb: FormBuilder) {
        this.measureFormArray = fb.array([])
    }

    ngOnInit(): void {

        this.measureList.forEach(m => {
            this.measureFormArray.push(this.fb.group(m))
        })
        this.parentGroup.setControl('listOfMeasures', this.measureFormArray)
    }

}

import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserWorkoutInstance} from "../../../share/entities/user-workout-instance";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
    selector: 'app-user-workout-instance',
    templateUrl: './user-workout-instance.component.html',
    styleUrls: ['./user-workout-instance.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserWorkoutInstanceComponent implements OnInit {

    @Input()
    userWorkoutInstance!: UserWorkoutInstance

    @Output()
    updateWorkoutInstance: EventEmitter<UserWorkoutInstance> = new EventEmitter<UserWorkoutInstance>()
    form!: FormGroup

    constructor(private fb: FormBuilder) {

    }

    ngOnInit(): void {
        this.form = this.fb.group(this.userWorkoutInstance)
    }


    onSubmit() {
        this.updateWorkoutInstance.emit(this.form.value)
    }
}

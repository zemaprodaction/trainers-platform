import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {WorkoutSet} from "../../../../share/entities/workout-set-entity";

@Component({
    selector: 'app-workout-set-list',
    templateUrl: './workout-set-list.component.html',
    styleUrls: ['./workout-set-list.component.css']
})
export class WorkoutSetListComponent implements OnInit {

    @Input()
    parentGroup!: FormGroup

    @Input()
    workoutSetList!: WorkoutSet[]
    workoutSetArrayControl!: FormArray

    constructor(private fb: FormBuilder) {
    }

    ngOnInit(): void {
        this.workoutSetArrayControl = this.fb.array([])
        this.workoutSetList.forEach(ws => {

            this.workoutSetArrayControl.push(this.fb.group({
                ...ws,
                restTime: this.fb.control(ws.restTime)
            }))
        })
        this.parentGroup.setControl('listOfSets', this.workoutSetArrayControl)
    }

}

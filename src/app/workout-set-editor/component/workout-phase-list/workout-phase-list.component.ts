import {Component, Input, OnInit} from '@angular/core';
import {WorkoutInstancePhaseEntity} from "../../../share/entities/workout-instance-phase-entity";
import {FormArray, FormBuilder, FormControl, FormGroup} from "@angular/forms";

@Component({
    selector: 'app-workout-phase-list',
    templateUrl: './workout-phase-list.component.html',
    styleUrls: ['./workout-phase-list.component.css']
})
export class WorkoutPhaseListComponent implements OnInit {

    @Input()
    workoutPhases!: WorkoutInstancePhaseEntity[]
    @Input()
    parent!: FormGroup
    workoutPhaseList!: FormArray

    constructor(private formBuilder: FormBuilder) {
    }

    ngOnInit(): void {
        this.workoutPhaseList = this.initWorkoutInstaneceListForm()
        this.parent.setControl('listOfWorkoutPhases', this.workoutPhaseList)
        console.log(this.parent);
    }

    private initWorkoutInstaneceListForm() {

        this.workoutPhaseList = this.formBuilder.array([]);
        this.workoutPhases.forEach((wp, index) => {
            const control = this.formBuilder.group(wp)
            this.workoutPhaseList.push(control)
        })
        return this.workoutPhaseList;
    }
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CsrfInterceptor} from "./interceptor/csrf.interceptor";
import {HTTP_INTERCEPTORS} from "@angular/common/http";


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        BrowserAnimationsModule,
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: CsrfInterceptor, multi: true},
    ],
})
export class CoreModule {
}

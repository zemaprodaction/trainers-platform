import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {StatisticEndpointConstant} from "../../share/endpointConstans";
import {UserWorkoutInstance} from "../../share/entities/user-workout-instance";


@Injectable({
    providedIn: 'root'
})
export class UserWorkoutInstanceApiService {

    constructor(private http: HttpClient) {
    }

    public getUserWorkoutInstance(workoutInstanceId: string) {
        return this.http.get<UserWorkoutInstance>(`${StatisticEndpointConstant.ROOT_WORKOUT_PROGRAM_SERVICE}/${workoutInstanceId}`)
    }

    updateWorkoutInstance(workoutInstance: UserWorkoutInstance) {
        return this.http.put<UserWorkoutInstance>(`${StatisticEndpointConstant.ROOT_WORKOUT_PROGRAM_SERVICE}`, workoutInstance)
    }
}

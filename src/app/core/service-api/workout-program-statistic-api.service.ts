import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CrudService} from "./crud.service";
import {StatisticEndpointConstant} from "../../share/endpointConstans";
import {CurrentWorkoutProgramStatistic} from "../../share/entities/current-workout-program-statistic";


@Injectable({providedIn: 'root'})
export class WorkoutProgramStatisticServiceApi extends CrudService<CurrentWorkoutProgramStatistic, string> {

  constructor(private http: HttpClient) {
    super(http, StatisticEndpointConstant.WORKOUT_PROGRAM_STATISTIC);
  }
}

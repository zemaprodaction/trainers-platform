import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CrudService} from "./crud.service";
import {WorkoutProgramEntity} from "../../share/entities/workout-programs";
import {WorkoutProgramApiConstants} from "../../share/endpointConstans";


@Injectable({providedIn: 'root'})
export class WorkoutProgramServiceApi extends CrudService<WorkoutProgramEntity, string> {

  constructor(private http: HttpClient) {
    super(http, WorkoutProgramApiConstants.WORKOUT_PROGRAM);
  }
}

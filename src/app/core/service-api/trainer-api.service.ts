import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {TrainersApiConstants} from "../../share/endpointConstans";
import {TrainerSubscription} from "../../share/entities/trainer-subscription";
import {User} from "../../share/entities/user";
import {Trainer} from "../../share/entities/trainer";


@Injectable({
  providedIn: 'root'
})
export class TrainerApiService {

  constructor(private http: HttpClient) { }

  public getTrainerSubscriptionUrl(){
    return this.http.get<TrainerSubscription>(`${TrainersApiConstants.GET_SUBSCRIBE_URL}`)
  }

  public getTrainerUsers(){
    return this.http.get<User[]>(`${TrainersApiConstants.GET_TRAINER_USERS}`)
  }

  public getTrainer(){
    return this.http.get<Trainer>(`${TrainersApiConstants.GET_TRAINER}`)
  }

}

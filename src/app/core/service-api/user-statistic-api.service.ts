import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {StatisticEndpointConstant} from "../../share/endpointConstans";
import {UserTrainingInfo} from "../../share/entities/user-training-info";
import {CurrentWorkoutProgramStatistic} from "../../share/entities/current-workout-program-statistic";


@Injectable({
    providedIn: 'root'
})
export class UserStatisticApiService {

    constructor(private http: HttpClient) {
    }

    public getUserCurrentWorkoutProgramStatistic(userId: string, workoutprogramId: string) {
        return this.http.get<CurrentWorkoutProgramStatistic>(`${StatisticEndpointConstant.WORKOUT_PROGRAM_STATISTIC}/${workoutprogramId}/${userId}`)
    }

    public getUserInfo(userId: string) {
        return this.http.get<UserTrainingInfo>(`${StatisticEndpointConstant.GET_USER_TRAINING_INFO}/${userId}`)
    }

}

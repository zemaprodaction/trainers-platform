import {HttpClient} from '@angular/common/http';
import {CrudOperations} from './crud-operations';
import {Observable} from 'rxjs';

export abstract class CrudService<T, ID> implements CrudOperations<T, ID> {

  protected constructor(
    protected _http: HttpClient,
    protected _base: string,
    protected _plural: string = ''
  ) {
  }

  save(t: T): Observable<T> {
    return this._http.post<T>(this._base, t);
  }

  update(id: ID, t: T): Observable<T> {
    return this._http.put<T>(this._base + '/' + id, t, {});
  }

  findOne(id: ID): Observable<T> {
    return this._http.get<T>(this._base + '/' + id);
  }

  findAll(): Observable<T[]> {
    return this._http.get<T[]>(this._base);
  }

  findByIdList(idList: ID[]): Observable<T[]> {
    return this._http.post<T[]>(this._base + '/' + this._plural, idList);
  }

  delete(id: ID): Observable<ID> {
    return this._http.delete<ID>(this._base + '/' + id);
  }

}

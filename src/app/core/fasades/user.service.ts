import {Injectable} from '@angular/core';
import {Store} from "@ngxs/store";
import {Observable} from "rxjs";
import {User} from "../../share/entities/user";
import {UserActions} from "../../shared-states/user-state/actions/user.actions";
import {UserState} from "../../shared-states/user-state/state/user.state";

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor(private store: Store) {
    }

    public addUsers(users: User[]) {
        this.store.dispatch(new UserActions.AddUsers(users))
    }
    public getUsers(): Observable<User[]> {
        return this.store.select(UserState.getUsers)
    }

}

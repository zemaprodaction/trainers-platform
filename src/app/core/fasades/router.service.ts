import {Injectable} from '@angular/core';
import {Store} from "@ngxs/store";
import {Navigate} from "@ngxs/router-plugin";

@Injectable({
    providedIn: 'root'
})
export class RouterService {

    constructor(private store: Store) {
    }

    public goToPage(url: string[]) {
        this.store.dispatch(new Navigate(url))
    }

}

import {Injectable} from '@angular/core';
import {Store} from "@ngxs/store";
import {AppActions} from "../store/actions/app.actions";
import {Observable} from "rxjs";
import {AppState} from "../store/states/app.state";
import {Trainer} from "../../share/entities/trainer";

@Injectable({
    providedIn: 'root'
})
export class TrainersService {

    constructor(private store: Store) {
    }

    public loadTrainerSubscriptionUrl() {
        this.store.dispatch(new AppActions.GetTrainerSubscribeUrl())
    }


    public loadTrainerUsers() {
        this.store.dispatch(new AppActions.GetTrainerUsers())
    }


    public loadTrainer() {
        this.store.dispatch(new AppActions.LoadTrainer())
    }

    public getTrainer(): Observable<Trainer> {
        return this.store.select(AppState.getTrainer)
    }

}

import {Injectable} from '@angular/core';
import {Store} from "@ngxs/store";
import {Observable} from "rxjs";
import {WorkoutProgramActions} from "../../shared-states/workout-program-state/actions/workout-program.actions";
import {WorkoutProgramState} from "../../shared-states/workout-program-state/state/workout-program.state";
import {WorkoutProgramEntity} from "../../share/entities/workout-programs";

@Injectable({
    providedIn: 'root'
})
export class WorkoutProgramService {

    constructor(private store: Store) {
    }

    public loadWorkoutProgram(workoutProgramId: string) {
        this.store.dispatch(new WorkoutProgramActions.LoadWorkoutProgram(workoutProgramId))
    }

    public getWorkoutProgram(workoutProgramId: string): Observable<WorkoutProgramEntity> {
        return this.store.select(WorkoutProgramState.getEntityById(workoutProgramId))
    }

}

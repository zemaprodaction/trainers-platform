import {Injectable} from '@angular/core';
import {Store} from "@ngxs/store";
import {Observable} from "rxjs";
import {UserTrainingInfoActions} from "../../shared-states/user-state/actions/user-training-info.actions";
import {CurrentWorkoutProgramStatisticActions} from "../../shared-states/current-workout-program-statistic/actions/current-workout-program-statistic";
import {UserTrainingInfoState} from "../../shared-states/user-state/state/user-training-info.state";
import {UserTrainingInfo} from "../../share/entities/user-training-info";
import {CurrentWorkoutProgramStatisticState} from "../../shared-states/current-workout-program-statistic/state/current-workout-program-statistic.state";
import {CurrentWorkoutProgramStatistic} from "../../share/entities/current-workout-program-statistic";

@Injectable({
    providedIn: 'root'
})
export class UserStatisticService {

    constructor(private store: Store) {
    }

    public loadTrainingInfo(userId: string) {
        this.store.dispatch(new UserTrainingInfoActions.LoadUserTrainingInfo(userId))
    }

    public loadUserWorkoutProgramCurrentStatistic(userId: string, workoutProgramId: string) {
        this.store.dispatch(new CurrentWorkoutProgramStatisticActions.LoadCurrentWorkoutProgramStatistic(userId, workoutProgramId))
    }

    public getUserTrainingInfo(userId: string): Observable<UserTrainingInfo> {
        return this.store.select(UserTrainingInfoState.getUsersTraingInfo(userId))
    }


    public getUserCurrentWorkoutProgramStatistic(userId: string): Observable<CurrentWorkoutProgramStatistic> {
        return this.store.select(CurrentWorkoutProgramStatisticState.selectCurrentWorkoutProgramStatisticByUserId(userId))
    }
}

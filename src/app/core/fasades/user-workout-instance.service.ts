import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {UserWorkoutInstanceApiService} from "../service-api/user-workout-instance-api.service";
import {UserWorkoutInstance} from "../../share/entities/user-workout-instance";

@Injectable({
    providedIn: 'root'
})
export class UserWorkoutInstanceService {

    constructor(private userWorkoutInstanceService: UserWorkoutInstanceApiService) {
    }

    public getUserWorkoutInstance(workoutInstanceId: string): Observable<UserWorkoutInstance> {
        return this.userWorkoutInstanceService.getUserWorkoutInstance(workoutInstanceId)
    }


    public updateUserWorkoutInstance(workoutInstance: UserWorkoutInstance): Observable<UserWorkoutInstance> {
        return this.userWorkoutInstanceService.updateWorkoutInstance(workoutInstance)
    }

}

import { TestBed } from '@angular/core/testing';

import { CsrfInterceptor } from './csrf.interceptor';
import {HttpXsrfTokenExtractor} from '@angular/common/http';

describe('CsrfInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      CsrfInterceptor,
      HttpXsrfTokenExtractor
      ]
  }));

  it('should be created', () => {
    const interceptor: CsrfInterceptor = TestBed.inject(CsrfInterceptor);
    expect(interceptor).toBeTruthy();
  });
});

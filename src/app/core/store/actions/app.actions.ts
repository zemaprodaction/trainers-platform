export namespace AppActions {
    export class GetTrainerSubscribeUrl {
        public static readonly type = '[App] Get Trainer Subscription Url'
    }

    export class GetTrainerUsers {
        public static readonly type = '[App] Get Trainer Users'
    }


    export class LoadTrainer {
        public static readonly type = '[App] Load Trainer'
    }
}

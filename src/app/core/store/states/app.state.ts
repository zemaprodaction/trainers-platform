import {Action, Selector, State, StateContext} from "@ngxs/store";
import {Injectable} from "@angular/core";
import {AppActions} from "../actions/app.actions";
import {TrainerApiService} from "../../service-api/trainer-api.service";
import {tap} from "rxjs/operators";
import {Trainer} from "../../../share/entities/trainer";

export interface AppStateModel {
    trainer: Trainer | null
}

@State<AppStateModel>({
    name: 'app',
    defaults: {
        trainer: null,
    }
})
@Injectable()
export class AppState {

    constructor(private trainerApiService: TrainerApiService) {
    }

    @Selector()
    public static getTrainer(state: AppStateModel) {
        if(state.trainer) {
            return state.trainer;
        } else{
            return {id: '', subscribeUrl: {subscriptionUrl: ''}, userList: []}
        }
    }

    @Action(AppActions.LoadTrainer)
    public loadTrainer(ctx: StateContext<AppStateModel>) {
        return this.trainerApiService.getTrainer().pipe(
            tap(trainer => ctx.patchState({
                trainer: trainer
            }))
        )
    }

}
